public interface IBeamReceiver
{
    /**
     * Notify object that it has collided with a projectile
     */
    void OnEnteredBeam(Beam CollidingBeam);
    void OnExitedBeam(Beam CollidingBeam);
}