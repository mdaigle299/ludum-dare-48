using Godot;

/**
 * Base class for every projectiles trajectory of the game
 */
public interface ProjectileTrajectory
{
    Vector2 ComputePosition(Vector2 ProjectilePosition, Vector2 Direction, float Speed, float DeltaTime);
}
