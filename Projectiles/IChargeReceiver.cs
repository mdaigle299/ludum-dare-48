using Godot;

public interface IChargeReceiver
{
    /**
     * Notify object that it has collided with a projectile
     */
    bool OnChargedByNode(Node2D ChargingNode, Vector2 ChargingDirection);
}