shader_type canvas_item;

uniform vec2 Direction = vec2(1.0,0.0);
uniform vec2 UVScale = vec2(1.0, 1.0);
uniform float Speed = 1.0;
uniform vec4 coolor = vec4(0.27, 1, 0.88, 1 );

void fragment()
{
	COLOR = texture(TEXTURE, (UV * UVScale) + (Direction * TIME * Speed));
	COLOR *= coolor;
}