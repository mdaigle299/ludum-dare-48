using Godot;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

public class Beam : Node2D
{
    private Sprite BackSprite;
    private ShaderMaterial BackMaterial;

    private Sprite MiddleSprite;
    private ShaderMaterial MiddleMaterial;
    private Sprite FrontSprite;
    private ShaderMaterial FrontMaterial;

    private Node2D BeamOffsetNode;

    private Vector2 End;

    private Node2D EndNode;
    private AudioStreamPlayer2D ChargingSound;
    private AudioStreamPlayer2D FireingSound;
    private AudioStreamPlayer2D ShutdownSound;
    private ReadOnlyCollection<Particles2D> ParticleFXs;

    public override void _Ready()
    {
        base._Ready();

        BackSprite = GetNode<Sprite>("BeamOffset/Background");
        BackMaterial = BackSprite.Material as ShaderMaterial;

        MiddleSprite = GetNode<Sprite>("BeamOffset/Middle");
        MiddleMaterial = MiddleSprite.Material as ShaderMaterial;

        FrontSprite = GetNode<Sprite>("BeamOffset/Foreground");
        FrontMaterial = FrontSprite.Material as ShaderMaterial;

        BeamOffsetNode = GetNode<Node2D>("BeamOffset");
        EndNode = GetNode<Node2D>("End");

        ChargingSound = GetNode<AudioStreamPlayer2D>("ChargingSound");
        FireingSound = GetNode<AudioStreamPlayer2D>("FireingSound");
        ShutdownSound = GetNode<AudioStreamPlayer2D>("ShutdownSound");

        List<Particles2D> temp = GetParticlesFXs(this);

        ParticleFXs = temp.AsReadOnly();
    }

    private List<Particles2D> GetParticlesFXs(Node node, List<Particles2D> particleFXs = null)
    {
        var children = node.GetChildren();
        var temp = particleFXs != null ? particleFXs : new List<Particles2D>();
        foreach (var child in children)
        {
            temp = GetParticlesFXs(child as Node, temp);

            if (child is Particles2D particles)
            {
                particles.Visible = false;
                temp.Add(particles);
            }
        }

        return temp;
    }

    public override void _Process(float delta)
    {
        base._Process(delta);

        UpdateUvScale(BeamOffsetNode.GlobalScale);
    }

    public void Charge()
    {
        ChargingSound.Play();
        FrontSprite.Visible = false;
        BackSprite.Visible = false;

        foreach(var p in ParticleFXs)
        {
            p.Visible = false;
        }
    }

    public void SetEnd(Vector2 NewEnd)
    {
        End = NewEnd;

        Vector2 Diff = End - GlobalPosition;

        // Update scale of beam
        BeamOffsetNode.GlobalScale = new Vector2(Diff.Length() / BackSprite.Texture.GetWidth(), 1.0f);
        GlobalRotation = End.AngleToPoint(GlobalPosition);

        EndNode.GlobalPosition = NewEnd;
    }

    public void Fire()
    {
        FireingSound.Play();
        FrontSprite.Visible = true;
        BackSprite.Visible = true;

        foreach(var p in ParticleFXs)
        {
            p.Visible = true;
        }        
    }

    private void UpdateUvScale(Vector2 NewScale)
    {
        BackMaterial.SetShaderParam("UVScale", NewScale);
        MiddleMaterial.SetShaderParam("UVScale", NewScale);
        FrontMaterial.SetShaderParam("UVScale", NewScale);
    }

}
