using Godot;
using System;

public class BeamCaster : BaseCaster
{
    enum States
    {
        Idle,
        Shooting,
        Cooldown,
        Charging
    }

    [Export]
    public PackedScene BeamScene;

    [Export]
    public float Duration = 1.0f;

    [Export]
    public NodePath RaycastPath;

    [Export]
    public bool AutoShoot = false;

    [Export]
    public float CooldownDuration = 1.0f;

    private float DurationAcc;

    private float CooldownAcc;

    private Beam CurrentBeam;

    private IBeamReceiver LastBeamReceiver;

    private RayCast2D Ray;

    private States CurrentState;
    private float ChargingAcc;

    [Export]
    public float ChargingDuration = 1.0f;

    public override void _Ready()
    {
        Ray = GetNode<RayCast2D>(RaycastPath);
        CurrentState = States.Idle;
    }

    public bool IsShooting { get { return CurrentState == States.Shooting; } }

    public bool CanShoot()
    {
        return !IsShooting;
    }

    protected override bool TryShootInternal()
    {
        if (CanShoot())
        {
            CurrentBeam = (Beam)BeamScene.Instance();
            CurrentBeam.GlobalPosition = GlobalPosition;

            GetTree().Root.AddChild(CurrentBeam);

            DurationAcc = 0.0f;
            ChargingAcc = 0.0f;
            CurrentState = States.Charging;
            CurrentBeam.Charge();

            return true;
        }

        return false;
    }

    public override void _Process(float delta)
    {
        base._Process(delta);

        switch (CurrentState)
        {
            case States.Idle:
                ProcessIdle(delta);
                break;
            case States.Cooldown:
                ProcessCooldown(delta);
                break;
            case States.Charging:
                ProcessCharging(delta);
                break;
            case States.Shooting:
                ProcessShooting(delta);
                break;
        }
    }

    private void ProcessCharging(float delta)
    {
        ChargingAcc += delta;

        if (ChargingAcc >= ChargingDuration)
        {
            CurrentState = States.Shooting;
            CurrentBeam.Fire();
        }
    }

    private void ProcessIdle(float delta)
    {
        if (AutoShoot)
        {
            TryShoot();
        }
    }

    private void ProcessCooldown(float delta)
    {
        CooldownAcc += delta;

        if (CooldownAcc >= CooldownDuration)
        {
            CurrentState = States.Idle;
            EmitSignal(nameof(cooldown_completed));
        }
    }

    private void ProcessShooting(float delta)
    {
        DurationAcc += delta;

        if (DurationAcc >= Duration)
        {
            if (LastBeamReceiver != null)
            {
                LastBeamReceiver.OnExitedBeam(CurrentBeam);
            }

            CurrentBeam.QueueFree();
            CurrentBeam = null;

            LastBeamReceiver = null;

            CurrentState = States.Cooldown;
            CooldownAcc = 0.0f;
        }
    }

    public override void _PhysicsProcess(float delta)
    {
        base._PhysicsProcess(delta);

        if (CurrentState == States.Shooting)
        {
            ShootCurrentBeam();
        }
        else if (CurrentState == States.Charging)
        {
            ChargeCurrentBeam();
        }
    }

    private void ChargeCurrentBeam()
    {
        Vector2 RayEndPoint;
        if (LastBeamReceiver != null)
        {
            LastBeamReceiver.OnExitedBeam(CurrentBeam);
            LastBeamReceiver = null;
        }

        if (Ray.IsColliding())
        {
            RayEndPoint = Ray.GetCollisionPoint();
        }
        else
        {
            RayEndPoint = Ray.GlobalTransform.Xform(Ray.CastTo);
        }

        CurrentBeam.SetEnd(RayEndPoint);
    }

    private void ShootCurrentBeam()
    {
        Vector2 RayEndPoint;

        if (Ray.IsColliding())
        {
            RayEndPoint = Ray.GetCollisionPoint();

            IBeamReceiver CurrentReceiver = Ray.GetCollider() as IBeamReceiver;

            if (CurrentReceiver != LastBeamReceiver)
            {
                if (LastBeamReceiver != null)
                {
                    LastBeamReceiver.OnExitedBeam(CurrentBeam);
                }

                CurrentReceiver?.OnEnteredBeam(CurrentBeam);
                LastBeamReceiver = CurrentReceiver;
            }
        }
        else
        {
            if (LastBeamReceiver != null)
            {
                LastBeamReceiver.OnExitedBeam(CurrentBeam);
                LastBeamReceiver = null;
            }

            RayEndPoint = Ray.GlobalTransform.Xform(Ray.CastTo);
        }

        CurrentBeam.SetEnd(RayEndPoint);
    }
}
