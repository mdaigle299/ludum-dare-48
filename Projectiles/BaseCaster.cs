using Godot;
using System;

public abstract class BaseCaster : Node2D
{

    [Export]
    public NodePath ShootSoundPath;
    private AudioStreamPlayer2D ShootSound;
    [Signal]
    public delegate void cooldown_completed();

    public override void _Ready()
    {
        if (ShootSoundPath != null)
        {
            ShootSound = GetNode<AudioStreamPlayer2D>(ShootSoundPath);
        }

    }

    public virtual bool TryShoot()
    {
        ShootSound?.Play();        
        return TryShootInternal();
    }

    protected abstract bool TryShootInternal();
}