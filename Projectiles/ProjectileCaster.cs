using Godot;
using System;

enum ProjectileCasterState
{
	Idle,
	Shooting,
	Cooldown
}
public class ProjectileCaster : BaseCaster
{
	[Export]
	public PackedScene ProjectileScene;

	[Export]
	public float CooldownDuration;

	[Export]
	public bool StartsInCooldown = false;

	[Export]
	public float InitialSpeed = 3.0f;

	[Export]
	public bool AutoShoot = false;

	[Export]
	public int NbRaffaleProjectile = 1;

	[Export]
	public float RaffaleCooldown = 0.25f;

	private float RaffaleCooldownAccumulator = 0.0f;
	
	private int RaffaleProjectileRemaining = 0;

	private float CooldownAccumulator;

	private ProjectileCasterState CurrentState = ProjectileCasterState.Idle;

	// Called when the node enters the scene tree for the first time.
	public override void _Ready()
	{
		base._Ready();
		CooldownAccumulator = 0.0f;
		CurrentState = StartsInCooldown ? ProjectileCasterState.Cooldown : ProjectileCasterState.Idle;

		Visible = false;
	}

	private void ProcessIdle(float delta)
	{
		if(AutoShoot)
		{
			TryShootInternal();
		}
	}
	
	private void ProcessCooldown(float delta)
	{
		CooldownAccumulator += delta;

		if(CooldownAccumulator >= CooldownDuration)
		{
			CurrentState = ProjectileCasterState.Idle;
			EmitSignal(nameof(cooldown_completed));
		}
	}

	private void ProcessShooting(float delta)
	{
		if(RaffaleProjectileRemaining == 0)
		{
			CurrentState = ProjectileCasterState.Cooldown;
		}
		else
		{
			RaffaleCooldownAccumulator += delta;
			if(RaffaleCooldownAccumulator >= RaffaleCooldown || RaffaleProjectileRemaining == NbRaffaleProjectile)
			{
				Shoot();
			}
		}
	}    

	public override void _Process(float delta)
	{
		base._Process(delta);

		switch(CurrentState)
		{
			case ProjectileCasterState.Idle:
				ProcessIdle(delta);
				break;
			case ProjectileCasterState.Shooting:
				ProcessShooting(delta);
				break;
			case ProjectileCasterState.Cooldown:
				ProcessCooldown(delta);
				break;
		}
		
	}

	/**
	 * Check if we can shoot
	 */
	public bool CanShoot()
	{
		return CurrentState == ProjectileCasterState.Idle;
	}

	/**
	 * Called to shoot a projectile
	 */
	protected override bool TryShootInternal()
	{
		if(CanShoot())
		{
			CurrentState = ProjectileCasterState.Shooting;
			RaffaleProjectileRemaining = NbRaffaleProjectile;
			CooldownAccumulator = 0.0f;

			return true;
		}

		return false;
	}

	private void Shoot()
	{
		Projectile NewProjectile = (Projectile)ProjectileScene.Instance();
		NewProjectile.Position = GlobalPosition;
		NewProjectile.Rotation = GlobalRotation;
		NewProjectile.Direction = new Vector2(1, 0).Rotated(GlobalRotation);
		NewProjectile.Speed = InitialSpeed;
 
		// Add the projectile to the scene
		GetTree().Root.AddChild(NewProjectile);
		--RaffaleProjectileRemaining;
		RaffaleCooldownAccumulator = 0.0f;
	}
}
