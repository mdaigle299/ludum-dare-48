public interface IProjectileReceiver
{
    /**
     * Notify object that it has collided with a projectile
     */
    void OnCollidedWithProjectile(Projectile CollidingProjectile);
}