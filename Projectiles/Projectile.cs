using Godot;

/**
 * Base class for every projectiles of the game
 */
public class Projectile : Node2D
{
	[Export]
	public NodePath HitArea2DPath;

	private Area2D HitArea;

	/**
	 * The number of time the projectile can collide with something before disapearing
	 */
	[Export]
	public int Life = 1;

	/**
	 * The duration of the projectile in seconds before it destroy itself
	 */
	[Export]
	public float Duration = 2.0f;

	[Export]
	public bool HasDuration = true;

	private float DurationAccumulator = 0.0f;

	public Vector2 Direction;

	public float Speed;

	public ProjectileTrajectory Trajectory;

	[Export] public NodePath TrajectoryNodePath;

	// Called when the node enters the scene tree for the first time.
	public override void _Ready()
	{
		base._Ready();


		HitArea = GetNode<Area2D>(HitArea2DPath);
		Trajectory = GetNode<ProjectileTrajectory>(TrajectoryNodePath);
		HitArea.Connect("area_entered", this, nameof(OnEnterArea));
	}

	public override void _Process(float delta)
	{
		if(HasDuration)
		{
			DurationAccumulator += delta;

			if(DurationAccumulator >= Duration)
			{
				DestroyProjectile();
			}
		}
		GlobalPosition = Trajectory.ComputePosition(GlobalPosition, Direction, Speed, delta);
		base._Process(delta);
	}

	/**
	 * Called when the projectile is about to being removed from scene
	 */
	protected void OnBeginDestroy()
	{

	}

	protected void DestroyProjectile()
	{
		OnBeginDestroy();
		// GetParent().RemoveChild(this);
		QueueFree();
	}

	protected void OnCollidedWithProjectileReceiver(IProjectileReceiver Receiver)
	{
		// Handle life
		--Life;

		if(Life <= 0)
		{
			DestroyProjectile();
		}
	}

	private void OnEnterArea(Area2D OtherArea)
	{
		ProcessCollisionWith((Node2D)(OtherArea.GetParent()));
	}

	/**
	 * Called to process collision with another object
	 */
	private void ProcessCollisionWith(Node2D CollidedObject)
	{
		if(CollidedObject is IProjectileReceiver)
		{
			IProjectileReceiver Receiver = (IProjectileReceiver)CollidedObject;

			// If projectile collide with a ProjectileReceiver, notify it of the collision
			if(Receiver != null)
			{
				Receiver.OnCollidedWithProjectile(this);

				OnCollidedWithProjectileReceiver(Receiver);
			}
		}
	}
}
