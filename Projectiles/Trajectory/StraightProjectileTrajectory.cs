using Godot;

public class StraightProjectileTrajectory : Node, ProjectileTrajectory
{

    public Vector2 ComputePosition(Vector2 ProjectilePosition, Vector2 Direction, float Speed, float DeltaTime)
    {
        ProjectilePosition += Direction * Speed * DeltaTime;
        return ProjectilePosition;
    }

}