using Godot;

public class SineProjectileTrajectory : Node, ProjectileTrajectory
{
	private float Time = 0.0f;
	[Export] public float Frequency = 4.0f;
	[Export] public float Amplitude = 40.0f;

	public Vector2 ComputePosition(Vector2 ProjectilePosition, Vector2 Direction, float Speed, float DeltaTime)
	{
		Vector2 Up = new Vector2(-Direction.y, Direction.x);

		float UpSpeed = Mathf.Cos(Time * Frequency) * Amplitude;
		Vector2 Velocity = Up * UpSpeed + Direction * Speed;

		ProjectilePosition += Velocity * DeltaTime;

		Time += DeltaTime;
		return ProjectilePosition;
	}

}
