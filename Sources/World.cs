using Godot;
using System;

public class World : Node2D
{
	public enum WorldState
	{
		Paused,
		Playing,
		JustDied,
		WaitingGameoverScreen,
		Gameover,
		Victory
	}

	[Export] public NodePath MainMenuNode;
	[Export] public NodePath GameoverNode;
	[Export] public NodePath VictoryNode;
	[Export] public PackedScene LevelOneScene;
	[Export] public NodePath GameUIScene;
	[Export] public NodePath PostProcessNode;

	private MainMenu MainMenu;
	private Gameover Gameover;
	private Victory Victory;
	private AudioStreamPlayer PauseSound;
	private Level LevelOne;
	private GameUI GameUI;
	private Sprite PostProcess;
	private Timer DeathTimer;

	[Export] public WorldState CurrentWorldState = WorldState.Paused;
	private bool MouseHidden = false;
	private bool PlayingInitialized;

	public override void _Ready()
	{
		var mouseCursor = GD.Load<Resource>("res://Assets/textures/target-cursor.png");
		Input.SetCustomMouseCursor(mouseCursor);

		MainMenu = GetNode<MainMenu>(MainMenuNode);
		Gameover = GetNode<Gameover>(GameoverNode);
		Victory = GetNode<Victory>(VictoryNode);
		PauseSound = GetNode<AudioStreamPlayer>("PauseSound");
		GameUI = GetNode<GameUI>(GameUIScene);
		PostProcess = GetNode<Sprite>(PostProcessNode);
		DeathTimer = new Timer();
		DeathTimer.WaitTime = 2;
		DeathTimer.Connect("timeout", this, nameof(DeathTimerTimeout));

		AddChild(DeathTimer);

		Gameover.OnRestartGame += OnRestartGame;
		Victory.OnPlayAgain += OnRestartGame;
		MainMenu.OnStartGame += OnStartGame;
		MainMenu.OnToggleSound += OnToggleSound;
	}

	private void OnToggleSound(object sender, bool toggled)
	{
		AudioServer.SetBusMute(0, toggled);
	}

	private void OnStartGame(object sender, EventArgs e)
	{
		CurrentWorldState = WorldState.Playing;
	}

	private void OnRestartGame(object sender, EventArgs e)
	{
		CurrentWorldState = WorldState.Paused;
	}

	public override void _Process(float delta)
	{
		switch (CurrentWorldState)
		{
			case WorldState.Paused:
				PauseState();
				break;
			case WorldState.JustDied:
				JustDiedState();
				break;
			case WorldState.Gameover:
				GameoverState();
				break;
			case WorldState.Playing:
				PlayingState();
				break;
			case WorldState.Victory:
				VictoryState();
				break;
		}
	}

	private void VictoryState()
	{
		if (MouseHidden)
		{
			Input.SetMouseMode(Input.MouseMode.Visible);
			MouseHidden = false;
		}

		Victory.Visible = true;

		// Hide main menu
		MainMenu.Visible = false;
		PostProcess.Visible = false;
		Gameover.Visible = false;

		if (LevelOne != null)
		{
			var score = LevelOne.LevelScore;
			Victory.ScoreText.Text = $"Score: {score}";

			LevelOne?.QueueFree();
			LevelOne = null;
			ClearAllMobs();
		}

		PlayingInitialized = false;
	}

	private void JustDiedState()
	{
		CurrentWorldState = WorldState.WaitingGameoverScreen;
		DeathTimer.Start();
	}

	private void DeathTimerTimeout()
	{
		CurrentWorldState = WorldState.Gameover;
		DeathTimer.Stop();
	}

	private void PlayingState()
	{
		if (Input.IsActionJustPressed("pause"))
		{
			PauseSound.Play();
			CurrentWorldState = WorldState.Paused;
		}

		if (!PlayingInitialized)
		{
			if (!MouseHidden)
			{
				Input.SetMouseMode(Input.MouseMode.Visible);
				Input.SetMouseMode(Input.MouseMode.Confined);
				MouseHidden = false;
			}

			// todo: start the level
			if (LevelOne == null)
			{
				var genericLevel = LevelOneScene.Instance();

				if (genericLevel is Level level)
				{
					LevelOne = level;
					AddChild(LevelOne);
					LevelOne.OnFail += OnLevelFailed;
				}
			}

			// Hide main menu
			MainMenu.Visible = false;
			PostProcess.Visible = false;
			Victory.Visible = false;

			PlayingInitialized = true;
		}
	}

	private void OnLevelFailed(object sender, EventArgs e)
	{
		CurrentWorldState = WorldState.JustDied;
	}

	private void GameoverState()
	{
		if (MouseHidden)
		{
			Input.SetMouseMode(Input.MouseMode.Visible);
			MouseHidden = false;
		}

		Gameover.Visible = true;

		// Hide main menu
		Victory.Visible = false;
		MainMenu.Visible = false;
		PostProcess.Visible = false;

		if (LevelOne != null)
		{
			var score = LevelOne.LevelScore;
			Gameover.SetScoreText($"Score: {score}");

			LevelOne?.QueueFree();
			LevelOne = null;
			ClearAllMobs();
		}
		PlayingInitialized = false;
	}

	private void PauseState()
	{
		if (MouseHidden)
		{
			Input.SetMouseMode(Input.MouseMode.Visible);
			MouseHidden = false;
		}

		// pause current level
		LevelOne?.QueueFree();
		LevelOne = null;
		ClearAllMobs();

		// hide restart menu
		Gameover.Visible = false;
		Victory.Visible = false;

		// Show main menu
		MainMenu.Visible = true;
		PostProcess.Visible = true;
		PlayingInitialized = false;
	}

	private void ClearAllMobs()
	{
		var children = GetTree().Root.GetChildren();

		foreach (var child in children)
		{
			if (child is Mob)
			{
				var mob = child as Mob;
				mob.QueueFree();
			}
		}
	}
}
