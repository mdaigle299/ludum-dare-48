using Godot;
using System;

public class Victory : Control
{
    public Label ScoreText;

    public event EventHandler OnPlayAgain;

    public override void _Ready()
    {
        ScoreText = GetNode<Label>("Panel/HBoxContainer/Label");
    }

    public void _on_Button_button_up()
    {
        OnPlayAgain?.Invoke(this, EventArgs.Empty);
    }
}
