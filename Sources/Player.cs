using Godot;
using System;

public class Player : KinematicBody2D, IAction, IProjectileReceiver, IChargeReceiver, IBeamReceiver
{
    [Export] public float Speed = 500;

    [Export] public float ChargeAnticipationTime = .2f;

    [Export] public Curve MovementStart;

    [Export] public Curve MovementEnd;

    [Export] public float TimeToReachMaximumVelocity = .5f;

    [Export] public float TimeToReachZeroVelocity = 0.2f;

    [Export] public bool LookAtMouse = true;

    [Export] public float MovementWhileChargeWeight = 0.05f;

    [Export] public float MinChargeStrenght = 2f;

    [Export] public float MaxChargeStrenght = 3f;

    [Export] public float MaxChargeHoldSec = 1f;

    [Export] public PackedScene DashPredictionScene;

    [Export] public float ReleaseSoundAmplifier = 10f;

    public Vector2 CurrentDirection = new Vector2();

    public Vector2 LastDirection = new Vector2();

    private bool IsCharging = false;

    private IAction Action = null;

    private TimedRepeater ChargeTimer;

    private float MovementMomentum;

    private float LastMovementMomentum;

    private float CurrentChargeHoldTime = 0.0f;

    private ChargeParticleController ChargeParticleController;
    private DashPrediction DashPrediction;

    private AnimatedSprite Face;
    private AnimationPlayer AnimationPlayer;

    // Debug Variables
    private bool Debug = false;

    private Label LockedLabel;

    private Line2D Line;

    private Area2D ChargingArea;

    public event EventHandler OnHit;

    private AudioStreamPlayer2D DeathSound;
    private AudioStreamPlayer2D HitSound;
    public AudioStreamPlayer2D ChargeSound;
    public AudioStreamPlayer2D ReleaseSound;
    private float OriginalReleaseSoundDb;
    private bool IsDead;
    private DateTime LastKillTime;
    public int KillCount = 0;
    [Export] public int KillComboTimer = 5000;
    public int KillComboCount;

    public event EventHandler OnKilledMob;
    public event EventHandler OnComboUp;

    public event EventHandler OnCharge;

    [Export] public int MaxLife = 3;
    public int CurrentLife;
    private TextureProgress StaminaProgress;
    [Export] public float ActionCooldown = 1.0f;
    private float ActionCooldownTimer = 0f;


    public override void _Ready()
    {
        DeathSound = GetNode<AudioStreamPlayer2D>("DeathSound");
        HitSound = GetNode<AudioStreamPlayer2D>("HitSound");
        ChargeSound = GetNode<AudioStreamPlayer2D>("ChargeSound");
        ReleaseSound = GetNode<AudioStreamPlayer2D>("ReleaseSound");
        OriginalReleaseSoundDb = ReleaseSound.VolumeDb;

        Action = this;

        this.ChargeParticleController = GetNodeOrNull<ChargeParticleController>("ChargeParticle");

        if (this.ChargeParticleController == null)
            GD.PrintErr("ChargeParticleController was not found in player, it will be deactivated");

        this.DashPrediction = this.DashPredictionScene.Instance() as DashPrediction;
        this.GetTree().Root.CallDeferred("add_child", DashPrediction);

        if (this.DashPrediction == null)
            GD.PrintErr("DashPrediction was not found in player, it will be deactivated");

        ChargingArea = GetNode<Area2D>("ChargingArea");
        ChargingArea.Connect("area_entered", this, nameof(OnEnteredHitArea));

        if (Debug)
        {
            this.LockedLabel = this.GetNode<Label>("MovementLockedVisual");
        }

        this.Face = this.GetNode<AnimatedSprite>("Face");

        AnimationPlayer = GetNode<AnimationPlayer>("AnimationPlayer");

        CurrentLife = MaxLife;

        StaminaProgress = GetNode<TextureProgress>("StaminaProgress");
        StaminaProgress.Visible = false;
    }

    public override void _ExitTree()
    {
        this.GetTree().Root.RemoveChild(DashPrediction);
    }

    private void OnEnteredHitArea(Area2D Area)
    {
        Node ParentNode = Area.GetParent();

        if (ParentNode is IChargeReceiver)
        {
            IChargeReceiver Receiver = (IChargeReceiver)ParentNode;

            // TODO: Detect that we are currently performing charge attack
            if (IsPlayerCharging())
            {
                var dead = Receiver.OnChargedByNode(this, LastDirection.Normalized());
                KilledMob(dead);
            }
        }
    }

    private void KilledMob(bool dead)
    {
        if (dead)
        {
            KillCount++;
            if ((DateTime.Now - LastKillTime).TotalMilliseconds < KillComboTimer)
            {
                KillComboCount++;
                OnComboUp?.Invoke(this, EventArgs.Empty);
            }
            else
            {
                KillComboCount = 1;
            }

            LastKillTime = DateTime.Now;

            OnKilledMob?.Invoke(this, EventArgs.Empty);
        }
    }

    public void ActionPressed(float delta)
    {
        if (!IsCharging) return;

        CurrentChargeHoldTime = Mathf.Min(CurrentChargeHoldTime + delta, MaxChargeHoldSec);

        if (ChargeParticleController != null)
        {
            var val = new ChargeParticleController.BoundedFloat(CurrentChargeHoldTime / MaxChargeHoldSec);
            ChargeParticleController.UpdateParticles(val);
        }

        Face.Animation = "Charging";
    }

    public void ActionJustPressed()
    {
        IsCharging = true;

        if (Debug)
        {
            this.LockedLabel.Visible = true;
        }

        ChargeSound.Play();

        if (ChargeParticleController != null)
        {
            ChargeParticleController.StartParticle();
        }

        if (DashPrediction != null)
            DashPrediction.StartAnimation();
    }

    public void ActionJustReleased()
    {
        if (!IsCharging) return;
        ChargeTimer = new TimedRepeater(ChargeAnticipationTime, 1, this.Charge);
    }

    private void Charge(int count)
    {
        IsCharging = false;

        ChargeTimer = null;

        this.LastDirection = (GetGlobalMousePosition() - this.GlobalPosition).Normalized();

        var diff = MaxChargeStrenght - MinChargeStrenght;

        var chargeMomentum = MinChargeStrenght + ((CurrentChargeHoldTime / MaxChargeHoldSec) * diff);

        var volume = OriginalReleaseSoundDb + (CurrentChargeHoldTime / MaxChargeHoldSec) * ReleaseSoundAmplifier;

        ReleaseSound.VolumeDb = volume;

        ReleaseSound.Play();
        AnimationPlayer.Play("Charge");

        OnCharge?.Invoke(this, EventArgs.Empty);

        MovementMomentum = chargeMomentum;
        LastMovementMomentum = chargeMomentum;

        CurrentChargeHoldTime = 0.0f;

        if (Debug)
        {
            this.LockedLabel.Visible = false;
        }

        if (ChargeParticleController != null)
        {
            ChargeParticleController.StopParticle();
        }

        if (DashPrediction != null)
            DashPrediction.StopAnimation();

        ActionCooldownTimer = ActionCooldown;
    }

    public override void _PhysicsProcess(float delta)
    {
        if (IsDead) return;

        if (!IsCharging)
        {
            if (this.IsPlayerCharging())
                Face.Animation = "Dashing";
            else if (MovementMomentum > 0)
                Face.Animation = "Idle";
        }

        if (ChargeTimer != null)
        {
            ChargeTimer._Process(delta);
        }

        if (!IsCharging)
        {
            var CurrentDirection = this.GetDirection();

            if (CurrentDirection != Vector2.Zero)
            {
                if (MovementMomentum <= 1)
                {
                    LastDirection = CurrentDirection;
                }
                else
                {
                    LastDirection = (LastDirection + (CurrentDirection * MovementWhileChargeWeight)).Normalized();
                }

                if (MovementMomentum <= 1)
                {
                    MovementMomentum = Mathf.Min(MovementMomentum + delta / TimeToReachMaximumVelocity, 1);
                }
                // After Charge slow down
                else
                {
                    MovementMomentum = Mathf.Max(MovementMomentum - delta / TimeToReachZeroVelocity, 0);
                }
            }
            else
            {
                MovementMomentum = Mathf.Max(MovementMomentum - delta / TimeToReachZeroVelocity, 0);
            }
        }
        else if (IsCharging)
        {
            MovementMomentum = Mathf.Max(MovementMomentum - delta / TimeToReachZeroVelocity, 0);
        }

        if (Action != null && ActionCooldownTimer <= 0f)
        {
            if (Input.IsActionPressed("action"))
                this.Action.ActionPressed(delta);
            if (Input.IsActionJustPressed("action"))
                this.Action.ActionJustPressed();
            if (Input.IsActionJustReleased("action"))
                this.Action.ActionJustReleased();
        }
        else if (Action == null)
            GD.PrintErr($"Action is null on {this.Name} it should never be null");


        if (MovementMomentum > 0)
        {
            Curve curve;

            if (MovementMomentum >= LastMovementMomentum)
                curve = MovementStart;
            else if (MovementMomentum < LastMovementMomentum)
                curve = MovementEnd;
            else
                GD.PrintErr("Wut?");

            var speed = Speed * MovementEnd.Interpolate(MovementMomentum);

            if (speed <= 1)
            {
                speed = Speed * MovementEnd.Interpolate(MovementMomentum);
            }
            else
            {
                speed = Speed * MovementMomentum;
            }

            LastMovementMomentum = MovementMomentum;

            this.MoveAndSlide(LastDirection * speed);
        }
        else
        {
            this.MoveAndSlide(Vector2.Zero);

        }

        if (LookAtMouse)
        {
            var mousePosition = (GetGlobalMousePosition() - this.GlobalPosition).Normalized();
            var forward = (this.Transform.x).Normalized();

            if (Debug)
            {
                if (Line == null)
                {
                    Line = new Line2D();
                    this.GetTree().Root.AddChild(Line);
                }

                Line.ClearPoints();

                Line.AddPoint(this.GlobalPosition + (forward * 150f));
                Line.AddPoint(this.GlobalPosition + (mousePosition * 150f));
            }

            this.LookAt(this.GlobalPosition + forward.LinearInterpolate(mousePosition, .05f));
        }

        if (DashPrediction != null)
        {
            var forward = (GetGlobalMousePosition() - this.GlobalPosition).Normalized();

            var perc = CurrentChargeHoldTime / MaxChargeHoldSec;
            var dist = (MaxChargeStrenght - MinChargeStrenght) * perc;
            DashPrediction.GlobalPosition = (forward * (100 + (225 * dist)) + this.GlobalPosition);
        }

        Face.FlipH = !(Vector2.Right.Dot((GetGlobalMousePosition() - this.GlobalPosition).Normalized()) > 0);

        if (ActionCooldownTimer > 0f)
        {
            ActionCooldownTimer -= delta;
            StaminaProgress.Visible = true;
            StaminaProgress.Value = ActionCooldownTimer / ActionCooldown * 100f;
        }
        else
        {
            StaminaProgress.Visible = false;
            StaminaProgress.Value = 0f;
        }
    }

    public void OnActionChange(IAction action)
    {
        if (action != null)
        {
            this.Action = action;
        }
        else
        {
            this.Action = this;
        }
    }

    private Vector2 GetDirection()
    {
        CurrentDirection = Vector2.Zero;

        if (Input.IsActionPressed("move_up"))
            CurrentDirection += Vector2.Up;

        if (Input.IsActionPressed("move_down"))
            CurrentDirection -= Vector2.Up;

        if (Input.IsActionPressed("move_left"))
            CurrentDirection += Vector2.Left;

        if (Input.IsActionPressed("move_right"))
            CurrentDirection -= Vector2.Left;

        return CurrentDirection.Normalized();
    }

    public void OnCollidedWithProjectile(Projectile CollidingProjectile)
    {
        if (IsPlayerCharging())
        {
            return;
        }

        Face.Animation = "Hit";
        HitSound.Play();

		CurrentLife--;
		if (CurrentLife <= 0)
		{
			Die();
		}

        OnHit?.Invoke(this, EventArgs.Empty);
    }

    public bool OnChargedByNode(Node2D ChargingNode, Vector2 ChargingDirection)
    {
        if (ChargingNode is Mob)
        {
            // TODO: Get further from mob
            // TODO: Reduce life
            Die();
            return true;
        }

        return false;
    }

    public void OnEnteredBeam(Beam CollidingBeam)
    {
        GD.Print("Player hit by beam");
        CurrentLife--;
        if (CurrentLife <= 0)
        {
            Die();
        }

		OnHit?.Invoke(this, EventArgs.Empty);
    }

    public void OnExitedBeam(Beam CollidingBeam)
    {
        // Do nothing
    }

    public bool IsPlayerCharging()
    {
        return MovementMomentum > 1.01f;
    }
    public void Die()
    {
        DeathSound.Play();
        AnimationPlayer.Play("Death");
        Face.Animation = "Death";
        IsDead = true;
        CurrentLife = 0;
    }

    public void Resurrect()
    {
        Visible = true;
    }
}
