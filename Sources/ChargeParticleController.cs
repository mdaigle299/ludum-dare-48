using Godot;
using System;

public class ChargeParticleController : Particles2D
{
	public class BoundedFloat
	{
		public readonly float MAX = 1;

		public readonly float MIN = 0;

		internal float Value;

		public BoundedFloat(float value)
		{
			Value = Mathf.Min(value, MAX);
			Value = Mathf.Max(Value, MIN);
		}
	}

	[Export] public float MaxSize = .6f;

	[Export] public float MinSize = .5f;

	[Export] public GradientTexture StartGradient;

	[Export] public GradientTexture EndGradient;
    

	public void UpdateParticles(BoundedFloat value)
	{
		var particlesMaterial = this.ProcessMaterial as ParticlesMaterial;

		var size = MinSize + ((MaxSize - MinSize) * value.Value);

		Texture gradient;

		if(value.Value < value.MAX - value.MAX * 0.01)
			gradient = StartGradient; 
		else
			gradient = EndGradient;

		particlesMaterial.Scale = size;
		particlesMaterial.ColorRamp = gradient;
	}

	public void StartParticle()
	{
		if(this.Emitting == false)
		{
			this.Emitting = true;
		}
		else
		{
			GD.Print($"{this.Name} int {this.GetParent().Name} was started while already emitting.");
		}
	}

	public void StopParticle()
	{
		if(this.Emitting == true)
		{
			this.Emitting = false;
		}
		else
		{
			GD.Print($"{this.Name} int {this.GetParent().Name} was stopped while already stopped.");
		}
	}

}
