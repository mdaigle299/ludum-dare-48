using System;
using Godot;

public class KingV : Node2D
{
	public Player PlayerPawn;
	public Shield Shield;
	//private Target Target;

	public event EventHandler<KingV> OnDeath;

	public override void _Ready()
	{
		PlayerPawn = GetNode<Player>("PlayerPawn");
		Shield = GetNode<Shield>("Shield");
		//Target = GetNode<Target>("Target");

		PlayerPawn.OnHit += OnPlayerHit;
	}

	private void OnPlayerHit(object sender, EventArgs args)
	{
		if (PlayerPawn.CurrentLife == 0)
		{
			OnDeath?.Invoke(this, this);
		}
	}

	public override void _Process(float delta)
	{
		Shield.GlobalPosition = PlayerPawn.GlobalPosition;
	}
}
