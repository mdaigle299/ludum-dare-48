using Godot;
using System;

public class Shield : Node2D, IProjectileReceiver, IBeamReceiver
{
    private Sprite Sprite;
    private float ShieldRotation;
    [Export] public float MaxSpeed = 150f;
    [Export] public float RotationAcceleration = 1f;
    [Export] public float DeadZoneDegrees = 1f;
    [Export] public Curve Curve;
    [Export] public float Radius = 64f;

    private float _angle = 30f;
    private bool CollisionShapeUpdated = false;

    [Export]
    public float Angle
    {
        get { return _angle; }
        set
        {
            _angle = value;
            CollisionShapeUpdated = true;
        }
    }

    public void Die()
    {
        Angle = 0f;
        Dead = true;
        AnimationPlayer.Play("Hit");
        AnimationPlayer.Queue("Die");
        ShieldDieSound.Play();
    }

    [Export] public string CurrentSprite = "Full";

    [Export] public float ShieldThickness = 0.75f;
    private float Speed = 0f;
    private int RotationDirection = 0;
    private string CurrentAnimation;
    private bool Idleing;
    private CollisionShape2D CollisionShape;
    private Area2D Area;
    private Sprite SpriteNone;
    private Sprite SpriteCritical;
    private Sprite SpriteGood;
    private Sprite SpriteFull;
    private AnimationPlayer AnimationPlayer;
    private AudioStreamPlayer2D ShieldHitSound;
    private AudioStreamPlayer2D ShieldDieSound;
    private Label Label;
    private uint? ShapeOwnerId;
    private string LastAnimation;
    private Vector2 LastMousePosition;
    private float LastMouseAngle;
    private bool ReachedSpeed;
    private float IdleCooldown = 0.7f;
    private float IdleTimer = 0.7f;
    public bool Dead = false;
    [Export] public int ShieldLife = 3;
    [Export] public bool Invulnerable = true;

    public event EventHandler OnHit;

    public override void _Ready()
    {
        Area = GetNode<Area2D>("Area2D");
        SpriteNone = GetNode<Sprite>("ShieldStatusSprites/None");
        SpriteCritical = GetNode<Sprite>("ShieldStatusSprites/Critical");
        SpriteGood = GetNode<Sprite>("ShieldStatusSprites/Good");
        SpriteFull = GetNode<Sprite>("ShieldStatusSprites/Full");
        AnimationPlayer = GetNode<AnimationPlayer>("ShieldStatusSprites/AnimationPlayer");
        ShieldHitSound = GetNode<AudioStreamPlayer2D>("ShieldHitSound");
        ShieldDieSound = GetNode<AudioStreamPlayer2D>("ShieldDieSound");
        Label = GetNode<Label>("Label");

        // creates a custom shape for collision
        InitArcCollisionShape(Vector2.Zero, Radius, 0f, Angle);
    }

    private void InitArcCollisionShape(Vector2 center, float radius, float angleFrom, float angleTo)
    {
        if (CollisionShape != null)
        {
            Area.RemoveChild(CollisionShape);
            CollisionShape.QueueFree();
        }

        CollisionShape = new CollisionShape2D();

        if (angleTo > 0f)
        {
            var resolution = 16;
            var points = new Vector2[resolution * 2];

            for (var i = 0; i < resolution; i++)
            {
                var anglePoint = Mathf.Deg2Rad(angleFrom + i * ((angleTo - angleFrom)) / resolution - angleTo / 2f);
                points[i] = new Vector2(Mathf.Cos(anglePoint), Mathf.Sin(anglePoint)) * radius;
            }

            for (var i = 0; i < resolution; i++)
            {
                var anglePoint = Mathf.Deg2Rad(angleFrom + (resolution - 1 - i) * (angleTo - angleFrom) / resolution - angleTo / 2f);
                points[i + resolution] = new Vector2(Mathf.Cos(anglePoint), Mathf.Sin(anglePoint)) * radius * (1 + (1 - ShieldThickness));
            }

            var poly = new ConvexPolygonShape2D();
            poly.Points = points;
            CollisionShape.Shape = poly;
        }

        Area.AddChild(CollisionShape);
    }

    public override void _Process(float delta)
    {
        if (CollisionShapeUpdated)
        {
            InitArcCollisionShape(Vector2.Zero, Radius, 0f, Angle);
            CollisionShapeUpdated = false;
        }

        if (!Dead)
        {
            ProcessShieldMovement(delta);
            UpdateSprite();
        }
    }

    private void UpdateSprite()
    {
        if (!Dead && CurrentSprite == "None")
        {
            AnimationPlayer.Queue("Die");
            Dead = true;
        }
    }

    private void ProcessShieldMovement(float delta)
    {
        LastAnimation = CurrentAnimation;

        var mousePosition = GetGlobalMousePosition();
        if (LastMousePosition != mousePosition)
        {
            LastMousePosition = mousePosition;
        }


        var angleToMouse = GetAngleTo(mousePosition);

        var distanceToMouse = LastMouseAngle - angleToMouse;

        Speed = Mathf.Clamp(Speed + RotationAcceleration, 0f, MaxSpeed);
        Speed = Speed * (1 + Mathf.Abs(distanceToMouse));
        var curve = Curve.Interpolate(Speed / MaxSpeed);
        var aimSpeed = Mathf.Deg2Rad(curve * MaxSpeed * delta);

        if (angleToMouse > Mathf.Deg2Rad(DeadZoneDegrees))
        {
            if (RotationDirection != 1)
            {
                Speed = 0f;
            }

            RotationDirection = 1;
            Rotation += aimSpeed;
            Idleing = false;
        }
        else if (angleToMouse < Mathf.Deg2Rad(-DeadZoneDegrees))
        {
            if (RotationDirection != -1)
            {
                Speed = 0f;
            }

            RotationDirection = -1;
            Rotation -= aimSpeed;

            Idleing = false;
        }
        else
        {
            Speed = Mathf.Clamp(Speed - RotationAcceleration * delta, 0, MaxSpeed);
            RotationDirection = 0;
        }

        if (LastAnimation != CurrentAnimation)
        {
            // AnimationPlayer.Play(CurrentAnimation);
        }

        var ratio = Mathf.Clamp(Speed / MaxSpeed, 0f, 1f);
        if (ratio > 0.3f && RotationDirection > 0)
        {
            SpriteFull.Frame = GetAnimationFrame((ratio - .3f) * .7f, 13, 7, 7);
            ReachedSpeed = true;
        }
        else if (ratio > 0.3f && RotationDirection < 0)
        {
            SpriteFull.Frame = GetAnimationFrame((ratio - .3f) * .7f, 20, 14, 7);
            ReachedSpeed = true;
        }
        else if (!Idleing && IdleCooldown <= 0f)
        {
            if (ReachedSpeed && RotationDirection > 0)
            {
                AnimationPlayer.Play("RightReverse");
                IdleCooldown = IdleTimer;
            }
            else if (ReachedSpeed && RotationDirection < 0)
            {
                AnimationPlayer.Play("LeftReverse");
                IdleCooldown = IdleTimer;
            }

            AnimationPlayer.Queue("Idle");
            Idleing = true;
            ReachedSpeed = false;
        }
        else if (IdleCooldown > 0f)
        {
            IdleCooldown -= delta;
        }

        LastMouseAngle = angleToMouse;
    }

    private int GetAnimationFrame(float ratio, int maxFrame, int minFrame, int numFrame)
    {
        var frame = 0;

        if (ratio < 0.5f)
        {
            frame = maxFrame - (int)(numFrame * (ratio / .5f));
        }
        else
        {
            frame = minFrame + (int)(numFrame * (ratio / .5f));
        }

        Label.Text = $"{frame.ToString()} {ratio / .5f}";

        return frame;
    }

    public void OnCollidedWithProjectile(Projectile CollidingProjectile)
    {
        ShieldHit();
    }

    private void ShieldHit()
    {
        if (!Invulnerable)
        {
            ShieldLife--;
        }

        if (ShieldLife > 0)
        {
            Hit();
        }
        else
        {
            Die();
        }

        OnHit?.Invoke(this, EventArgs.Empty);
    }

    public void Hit()
    {
        AnimationPlayer.Play("Hit");
        ShieldHitSound.Play();
        AnimationPlayer.Queue("Idle");
        IdleCooldown = IdleTimer;
    }

    public void OnEnteredBeam(Beam CollidingBeam)
    {
        ShieldHit();
    }

    public void OnExitedBeam(Beam CollidingBeam)
    {
        // Do nothing
    }
}
