public interface IAction
{
    void ActionJustPressed();

    void ActionJustReleased();

    void ActionPressed(float delta);
}