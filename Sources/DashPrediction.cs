using Godot;
using System;

public class DashPrediction : Node2D
{
	private AnimationPlayer DashAnimation;

	public override void _Ready()
	{
		base._Ready();
		this.DashAnimation = GetNode<AnimationPlayer>("DashAnimation");
		StopAnimation();
	}

	public void StartAnimation()
	{
		this.DashAnimation.CurrentAnimation = "DashPrediction";
		this.Visible = true;
	}

	public void StopAnimation()
	{
		this.DashAnimation.Stop(true);
		this.Visible = false;
	}
}
