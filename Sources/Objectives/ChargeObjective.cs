using System;
using Godot;
public class ChargeObjective : IObjective
{
    private StaticBody2D GlotteStopper;

    public AnimationPlayer AnimationPlayer { get; private set; }
    public Player Player { get; private set; }

    private bool Charged;

    public string GetObjective()
    {
        return $"Objective\nHold [Left Click] to charge\nAim at the glottis using the mouse.";
    }

    public ChargeObjective(StaticBody2D glotteStopper, AnimationPlayer animationPlayer, Player player)
    {
        GlotteStopper = glotteStopper;
        AnimationPlayer = animationPlayer;
        Player = player;

        Player.OnCharge += OnCharge;
    }

    private void OnCharge(object sender, EventArgs e)
    {
        var chargeDirection = Player.LastDirection;
        if (chargeDirection.x > 0)
        {
            Charged = true;
            GlotteStopper.QueueFree();
            Player.OnCharge -= OnCharge;
        }
    }

    public void Update(float delta)
    {
    }

    public bool IsDone()
    {
        return Charged;
    }
}
