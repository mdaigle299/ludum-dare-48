using Godot;

public class PlayerMovedDistanceObjective : IObjective
{
	public string ObjectiveText = "";
	private Vector2 BasePosition;
	
	private float Distance;

	private Player Player;

	public string GetObjective()
	{
		return $"Objective : Move King V using W, A, S, and D.";
	}

	public PlayerMovedDistanceObjective(Player player, float distance)
	{
		BasePosition = player.GlobalPosition;
		Player = player;
		Distance = distance;
	}

	public void Update(float delta)
	{
	}

	public bool IsDone()
	{
		return Mathf.Abs((BasePosition - Player.GlobalPosition).Length()) > Distance;
	}
}
