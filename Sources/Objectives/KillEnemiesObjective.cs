using System.Collections.Generic;
using Godot;

public class KillEnemiesObjective : Node, IObjective
{
	private int EnemiesAlive;

	private int EnemiesTotal;

	public string GetObjective()
	{
		return $"Objective : Kill enemies. {EnemiesAlive}/{EnemiesTotal} alive.";
	}

	public KillEnemiesObjective(List<Mob> mobs)
	{
		EnemiesAlive = mobs.Count;
		EnemiesTotal = mobs.Count;

		foreach(Mob mob in mobs)
		{
			mob.Connect("death_finished", this, "EnemyDied");
		}
	}

	public void Update(float delta)
	{
	}

	public bool IsDone()
	{
		return EnemiesAlive <= 0;
	}

	public void EnemyDied()
	{
		EnemiesAlive -= 1;
	}
}
