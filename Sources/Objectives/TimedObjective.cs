using System;

public class TimedObjective : IObjective
{
    private float CurrentTime;

    private float TotalTime;

	public string GetObjective()
	{
		return $"Objective : Survive for {Math.Round(TotalTime - CurrentTime, 2)}";
	}

    public TimedObjective(float timeInSeconds)
    {
        TotalTime = timeInSeconds;
    }

    public void Update(float delta)
    {
        CurrentTime += delta;
    }

    public bool IsDone()
    {
        return CurrentTime >= TotalTime;
    }
}