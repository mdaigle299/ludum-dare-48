public class VictoryObjective : IObjective
{
    private readonly World World;

    public string GetObjective()
    {
        return $"Victory!";
    }

    public VictoryObjective(World world)
    {
        World = world;
        World.CurrentWorldState = World.WorldState.Victory;
    }

    public void Update(float delta)
    {
    }

    public bool IsDone()
    {
        return true;
    }
}
