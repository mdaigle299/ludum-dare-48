public interface IObjective
{
	string GetObjective();

	void Update(float delta);
	
	bool IsDone();
}
