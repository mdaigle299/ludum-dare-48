using Godot;
using System;

public class MainMenu : Control
{
	private AudioStreamPlayer ClickSound;

	public event EventHandler OnStartGame;
	public event EventHandler<bool> OnToggleSound;

	public override void _Ready()
	{
		ClickSound = GetNode<AudioStreamPlayer>("ClickSound");
	}

	private void _on_StartGame_button_up()
	{
		ClickSound.Play();
		OnStartGame?.Invoke(this, null);
	}

	private void _on_Quit_button_up()
	{
		ClickSound.Play();
		GetTree().Quit();
	}

	private void _on_NoSound_toggled(bool toggled)
	{
		ClickSound.Play();
		OnToggleSound?.Invoke(this, toggled);
	}
}


