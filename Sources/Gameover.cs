using Godot;
using System;

public class Gameover : Control
{
	private Label LevelScoreText;

	public event EventHandler OnRestartGame;
	private void _on_Restart_button_up()
	{
		OnRestartGame?.Invoke(this, null);
	}

	public override void _Ready()
	{
		LevelScoreText = GetNode<Label>("Panel/HBoxContainer/Score");
	}

	public void SetScoreText(string text)
	{
		LevelScoreText.Text = text;
	}
}
