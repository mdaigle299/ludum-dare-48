using Godot;
using System;

public class EnemySpawner : Node2D
{

    [Signal]
    public delegate void mob_spawned(Mob SpawnedMob);

    [Export]
    public PackedScene MobScene;

    private Mob CurrentMob;

    [Export]
    public PackedScene BrainScene;

    private Brain CurrentBrain;

    [Export]
    public NodePath NodeToFollowPath;

    private Node2D NodeToFollow;

    private bool Spawned = false;

    public void SetBrain(Brain brain)
    {
        SpawnBrain(brain);
    }

    public override void _Process(float delta)
    {
        base._Process(delta);

        if (!Spawned)
        {
            SpawnBrain();
        }
        else
        {
            if (CurrentMob.CanMove() && NodeToFollow != null)
            {
                CurrentMob.GoTo(NodeToFollow.GlobalPosition);
            }
        }
    }

    private void SpawnBrain(Brain brain = null)
    {
        CurrentBrain = brain ?? (Brain)BrainScene.Instance();
        GetTree().Root.AddChild(CurrentBrain);

        SpawnMob();
    }

    private void SpawnMob()
    {
        CurrentMob = (Mob)MobScene.Instance();
        CurrentMob.GlobalPosition = GlobalPosition;
        CurrentMob.GlobalRotation = GlobalRotation;
        CurrentMob.BrainPath = CurrentBrain.GetPath();
        GetTree().Root.AddChild(CurrentMob);

        if (NodeToFollowPath != null && !NodeToFollowPath.IsEmpty())
        {
            NodeToFollow = GetNode<Node2D>(NodeToFollowPath);
            CurrentMob.GlobalPosition = NodeToFollow.GlobalPosition;
        }

        EmitSignal(nameof(mob_spawned), CurrentMob);

        Spawned = true;
    }

    public Mob GetCurrentMob()
    {
        return CurrentMob;
    }
}
