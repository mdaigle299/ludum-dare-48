using Godot;
using System;

public class BeamFollowMouse : Node2D
{
	[Export]
	public NodePath BeamPath;

	private Beam Laser;

	// Declare member variables here. Examples:
	// private int a = 2;
	// private string b = "text";

	// Called when the node enters the scene tree for the first time.
	public override void _Ready()
	{
		base._Ready();

		Laser = GetNode<Beam>(BeamPath);
	}

	public override void _Process(float delta)
	{
		base._Process(delta);

		Laser.SetEnd(GetGlobalMousePosition());
	}
}
