using Godot;

public class WhiteCell_1Direction_Spawner_NoMove_Laser : Node2D
{
    [Export] public NodePath TargetPath;
    private EnemySpawner Spawner;
    private Node2D Target;
    [Export] public PackedScene ShootStaticTargetBrainScene;
    public override void _Ready()
    {
        Spawner = GetNode<EnemySpawner>("Spawner");
        Target = GetNode<Node2D>(TargetPath);
        var brain = (ShootStaticTargetBrain)ShootStaticTargetBrainScene.Instance();
        brain.Target = Target;

        Spawner.SetBrain(brain);
    }
}
