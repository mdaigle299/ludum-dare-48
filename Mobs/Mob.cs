using Godot;
using System;

public abstract class Mob : KinematicBody2D
{
	// Declare member variables here. Examples:
	// private int a = 2;
	// private string b = "text";
	[Export]
	public NodePath BrainPath;

	[Export]
	public NodePath SensingAreaPath;

	private Area2D SensingArea_;

	public Area2D SensingArea { get { return SensingArea_; } }

	private Brain CurrentBrain_;

	public Brain CurrentBrain { get { return CurrentBrain_; } }

	public bool IsDieing = false;

	[Export]
	public NodePath DieSoundPath;
	private AudioStreamPlayer2D DieSound;

	[Signal]
	public delegate void charge_finished();

	[Signal]
	public delegate void death_finished();

	[Signal]
	public delegate void shoot_finished();


	// Called when the node enters the scene tree for the first time.
	public override void _Ready()
	{
		base._Ready();

		SensingArea_ = GetNode<Area2D>(SensingAreaPath);

		CurrentBrain_ = GetNode<Brain>(BrainPath);
		CurrentBrain_.SetMob(this);

		if (DieSoundPath != null)
		{
			DieSound = GetNode<AudioStreamPlayer2D>(DieSoundPath);
		}
	}

	public virtual bool CanMove()
	{
		return true;
	}

	public virtual bool IsCurrentlyCharging()
	{
		return false;
	}

	public abstract void GoTo(Vector2 TargetLocation);

	// Call by brain to notify mob to charge at location
	public abstract bool TryCharge(Vector2 TargetLocation);

	// Call by brain to notify mob to shoot at location
	public abstract bool TryShoot(Vector2 TargetLocation);

	// Call by brain to notify the mob to die
	public virtual void Die()
	{
		this.IsDieing = true;
		DieSound?.Play();
	}
}
