using Godot;
using System;
using Godot.Collections;

public class WhiteCellMob : Mob, IProjectileReceiver, IChargeReceiver
{
	[Export]
	public PackedScene ExplosionScene;

	[Export]
	public float MaxSpeed = 600.0f;

	// Declare member variables here. Examples:
	// private int a = 2;
	// private string b = "text";

	[Export]
	public NodePath ProjectileCastersRootPath; 

	[Export]
	public bool RotateProjectileCastersTowardPlayer = true;

	[Export] 
	public NodePath FacePath;

	private Godot.Collections.Array<BaseCaster> MainProjectileCasters;

	private Charger ChargeAttack;

	private Area2D ChargingArea;

	private Node2D ProjectileOffsetNode;

	private AnimationPlayer Animation;

	private Player SensedPlayer;
    private Particles2D CoinsParticles;
    private Timer DeathTimer;
    private bool IsCharging;
	private bool IsShooting;

	private bool IsDying;

	private Vector2 GotoDestination;
	private bool IsMoving;

	private Vector2 ChargeDirection;

	private int NbProjectileCasterInCooldown = 0;

	private AnimatedSprite Face;

	// Called when the node enters the scene tree for the first time.
	public override void _Ready()
	{
		base._Ready();

		ProjectileOffsetNode = GetNode<Node2D>("ProjectileOffset");
		this.Face = GetNode<AnimatedSprite>(FacePath);

		MainProjectileCasters = new Godot.Collections.Array<BaseCaster>();
		Node ProjectileCasterRoot = GetNode(ProjectileCastersRootPath);
		foreach(Node ProjectileCasterNode in ProjectileCasterRoot.GetChildren())
		{
			BaseCaster ChildProjectileCaster = ProjectileCasterNode as BaseCaster;

			if(ChildProjectileCaster != null)
			{
				MainProjectileCasters.Add(ChildProjectileCaster);
				ChildProjectileCaster.Connect("cooldown_completed", this, nameof(OnShootingCooldownCompleted));
			}
		}


		ChargeAttack = GetNode<Charger>("Charger");
		ChargeAttack.Connect("cooldown_completed", this, nameof(OnChargeCooldownCompleted));

		ChargingArea = GetNode<Area2D>("ChargeHitArea");
		ChargingArea.Connect("area_entered", this, nameof(OnChargeAreaEntered));

		Animation = GetNode<AnimationPlayer>("AnimationPlayer");

		CoinsParticles = GetNode<Particles2D>("Coins_Particles");
		
		DeathTimer = GetNode<Timer>("DeathTimer");
		DeathTimer.Connect("timeout", this, nameof(DeathTimerTimeout));

		IsCharging = false;
		IsShooting = false;
		IsDying = false;
		IsMoving = false;
	}

	private void OnChargeAreaEntered(Area2D EnteredArea)
	{
		Node ParentNode = EnteredArea.GetParent();

		if(ParentNode is IChargeReceiver)
		{
			IChargeReceiver Receiver = (IChargeReceiver)ParentNode;

			if(IsCharging)
			{
				Receiver.OnChargedByNode(this, ChargeDirection);

				if(ParentNode is Player)
				{
					Die();
					IsCharging = false;
				}
			}
		}
	}

	public override void _Process(float delta)
	{
		base._Process(delta);

		if(IsMoving)
		{
			Vector2 Distance = GotoDestination - GlobalPosition;

			if(Distance.Length() <= MaxSpeed * delta)
			{
				GlobalPosition = GotoDestination;
				IsMoving = false;
			}
			else
			{
				GlobalPosition += Distance.Normalized() * MaxSpeed * delta;
			}
		}
		
		if(IsMoving)
			this.Face.Animation = "Idle";
		else if (IsDying)
			this.Face.Animation = "Death";
		else if (IsShooting)
			this.Face.Animation = "Charging";
		else if (IsCharging)
			this.Face.Animation = "Charging";
	}

	public override void GoTo(Vector2 TargetLocation)
	{
		GotoDestination = TargetLocation;
		IsMoving = true;
	}

	public override bool TryShoot(Vector2 TargetLocation)
	{
		if(IsDying)
		{
			return false;
		}

		if(MainProjectileCasters.Count == 0)
		{
			return false;
		}

		if(RotateProjectileCastersTowardPlayer)
		{
			ProjectileOffsetNode.GlobalRotation = TargetLocation.AngleToPoint(GlobalPosition);
		}

		Animation.Play("Shoot");

		IsShooting = true;
		IsMoving = false;

		return true;
	}

	public void ShootProjectiles()
	{
		NbProjectileCasterInCooldown = 0;
		
		foreach(BaseCaster Caster in MainProjectileCasters)
		{
			if(Caster.TryShoot())
			{
				NbProjectileCasterInCooldown++;
			}
		}
	}

	private void OnShootingCooldownCompleted()
	{
		--NbProjectileCasterInCooldown;

		if(NbProjectileCasterInCooldown == 0)
		{
			IsShooting = false;
			EmitSignal(nameof(shoot_finished));
		}
	}

	public override bool TryCharge(Vector2 TargetLocation)
	{
		if(IsDying)
		{
			return false;
		}

		ChargeDirection = (TargetLocation - GlobalPosition).Normalized();

		Animation.Play("Charge");

		IsCharging = true;
		IsMoving = false;

		return true;
	}

	public void LaunchChargeAttack()
	{
		ChargeAttack.Charge(ChargeDirection);
	}

	private void OnChargeCooldownCompleted()
	{
		IsCharging = false;
		EmitSignal(nameof(charge_finished));
	}

	public override void Die()
	{
		base.Die();
		CoinsParticles.Emitting = true;
		Animation.Play("Death");
		DeathTimer.Start();
		IsDying = true;
		IsMoving = false;
	}

	public void DeathTimerTimeout()
	{
		EmitSignal(nameof(death_finished));
		QueueFree();
	}

	public void SpawnExplosion()
	{
		Node2D ExplosionNode = (Node2D)ExplosionScene.Instance();
		ExplosionNode.Position = GlobalPosition;
		
		GetParent().AddChild(ExplosionNode);
	}

	public void OnCollidedWithProjectile(Projectile CollidingProjectile)
	{
		// TODO: Handle projectile
	}

	public override bool CanMove()
	{
		return !(IsShooting || IsCharging || IsDying);
	}

	public bool OnChargedByNode(Node2D ChargingNode, Vector2 ChargingDirection)
	{
		if(ChargingNode is Player)
		{
			if(!(IsCharging || IsDying))
			{
				IsDying = true;
				Die();
			}
		}

		return IsDying;
	}

	public override bool IsCurrentlyCharging()
	{
		return IsCharging;
	}
}
