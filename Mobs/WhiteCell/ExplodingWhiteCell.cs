using Godot;
using System;

public class ExplodingWhiteCell : Node2D
{
	private Particles2D Particles;

	// Called when the node enters the scene tree for the first time.
	public override void _Ready()
	{
		Particles = GetNode<Particles2D>("Particles2D");

		Particles.Emitting = true;
	}

	public override void _Process(float delta)
	{
		if(Particles.Emitting == false)
		{
			GetParent().RemoveChild(this);
		}
	}
}
