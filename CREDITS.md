Martin Daigle (@martin-daigle) Programming
Giranath (@giranath) Programming
Sharpjuice (@sharpjuice)
Frédérik Charest (@allov) Programming, Music and Sounds
NoLuck (@noluck) Visuals
Hugo Lefevre (@hugo-lefevre) Level design

Sounds:
https://freesound.org/people/LegoLunatic/sounds/151243/
https://freesound.org/people/JonnyRuss01/sounds/478196/
https://freesound.org/people/lolamadeus/sounds/159653/
https://freesound.org/people/UOregonCinemaStudies/sounds/418967/
https://freesound.org/people/biawinter/sounds/408098/
https://freesound.org/people/simosco/sounds/235555/
