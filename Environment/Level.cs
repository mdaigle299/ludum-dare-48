using Godot;
using System;
using System.Collections.Generic;
using Godot.Collections;

public class Level : Node2D
{
	[Export] public NodePath AnimationPlayerPath;

	private AnimationPlayer AnimationPlayer;
    private KingV KingV;
    private Player Player;
	
	private List<IObjective> CurrentObjectives = new List<IObjective>();

	private GameUI GameUI;
    private StaticBody2D GlotteStopper;
    [Export] public int KillAwardPoints = 1000;
    public int LevelScore = 0;

    public event EventHandler OnFail;

	// Called when the node enters the scene tree for the first time.
	public override void _Ready()
	{
		AnimationPlayer = GetNodeOrNull<AnimationPlayer>(AnimationPlayerPath);
		KingV = GetNodeOrNull<KingV>("KingV");
		Player = KingV?.PlayerPawn;

		if (KingV == null || Player == null)
		{
			GD.PrintErr("Level needs a KingV object name 'KingV' at the root of the scene.");
			throw new ArgumentNullException("KingV");
		}

		KingV.OnDeath += OnKingVDeath;
		Player.OnKilledMob += OnKilledMob;
		Player.OnComboUp += OnComboUp;
		Player.OnHit += OnHit;

		GameUI = this.GetTree().Root.GetNodeOrNull<GameUI>("World/CanvasLayer/GameUI");

		GameUI.UpdateHearts(Player.CurrentLife, Player.MaxLife);

		GlotteStopper = GetNode<StaticBody2D>("Boss/GlotteObstacle");
	}

    private void OnHit(object sender, EventArgs e)
    {
		GameUI.UpdateHearts(Player.CurrentLife, Player.MaxLife);
    }

    private void OnComboUp(object sender, EventArgs e)
    {
        LevelScore += KillAwardPoints * Player.KillComboCount;
		if (Player.KillComboCount > 1)
		{
			GameUI.SetKillCountText($"{Player.KillComboCount}x Combo!");
		}
    }

    private void OnKilledMob(object sender, EventArgs e)
    {
        LevelScore += KillAwardPoints;
		GameUI.UpdateKillScore(LevelScore);
    }

    private void OnKingVDeath(object sender, KingV e)
    {
        OnFail?.Invoke(this, EventArgs.Empty);
    }

    // Called every frame. 'delta' is the elapsed time since the previous frame.
    public override void _Process(float delta)
	{
		var objectivesCompleted = true;

		foreach(IObjective objective in CurrentObjectives)
		{
			objective.Update(delta);

			if(objectivesCompleted == true)
			{
				objectivesCompleted = objective.IsDone();
			}
		}

		AnimationPlayer.PlaybackActive = objectivesCompleted;
		
		if(objectivesCompleted && CurrentObjectives.Count > 0)
		{
			CurrentObjectives.Clear();
		}

		if(GameUI != null)
		{
			if(CurrentObjectives.Count > 0)
			{
				GameUI.SetObjectiveText(CurrentObjectives[0].GetObjective());
			}
			else
			{
				GameUI.SetObjectiveText("");
			}
		}
	}
	
	public void TimedObjective(float time)
	{
		CurrentObjectives.Add(new TimedObjective(time));
	}

	public void KillEnemiesObjective(Godot.Collections.Array<NodePath> mobPaths)
	{
		var mobs = new List<Mob>();

		for (int i = 0; i < mobPaths.Count; i++)
		{
			var spawner = GetNodeOrNull<EnemySpawner>(mobPaths[i]+ "/Spawner");

			if(spawner != null)
			{
				var mob = spawner.GetCurrentMob();
				if(mob != null && !mob.IsDieing)
				{
					mobs.Add(mob);
				}
			}
		}

		if(mobs.Count != 0)
		{
			CurrentObjectives.Add(new KillEnemiesObjective(mobs));
		}
	}

	public void PlayerMovedDistanceObjective(float distance)
	{
		var mobs = new List<Mob>();
		// Get all mobs in the map to clear
		CurrentObjectives.Add(new PlayerMovedDistanceObjective(this.Player, distance));
	}

	public void ChargedObjective()
	{
		CurrentObjectives.Add(new ChargeObjective(GlotteStopper, AnimationPlayer, Player));
	}

	public void VictoryObjective()
	{
		CurrentObjectives.Add(new VictoryObjective(GetTree().Root.GetNode<World>("World")));
	}
}
