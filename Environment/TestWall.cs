using Godot;
using System;

public class TestWall : Node2D, IProjectileReceiver
{
	public void OnCollidedWithProjectile(Projectile CollidingProjectile)
	{
		GD.Print("Wall collided with projectile");
	}

	// Declare member variables here. Examples:
	// private int a = 2;
	// private string b = "text";

	// Called when the node enters the scene tree for the first time.
	public override void _Ready()
	{
		
	}

//  // Called every frame. 'delta' is the elapsed time since the previous frame.
//  public override void _Process(float delta)
//  {
//      
//  }


}
