using Godot;
using System;

public class Charger : Node
{
	[Export]
	public NodePath ChargingNodePath;

	[Export]
	public NodePath ChargeHitAreaPath;

	[Export]
	public float InitialMomentum = 100.0f;

	[Export] 
	public Curve MomentumDampeningCurve;

	[Export]
	public float DampeningDuration = 2.0f;

	[Export]
	public float MaxDampening = 100.0f;

	[Export]
	public float CooldownDuration = 3.0f;

	/**
	 * Check if the charger is currently charging
	 */
	public bool Charging { get { return IsCharging; }}
	private bool IsCharging;

	private Vector2 ChargeDirection;

	private Node2D ChargingNode;

	private Area2D ChargeHitArea;

	private float Momentum;

	private float CooldownAcc;

	private bool IsCoolingDown;

	[Signal]
	public delegate void charge_finished();

	[Signal]
	public delegate void charge_started();

	[Signal]
	public delegate void charge_cancelled();

	[Signal]
	public delegate void cooldown_completed();

	/**
	 * Signal emitted each time the charger hit something
	 */
	[Signal]
	public delegate void charge_hit();

	private float ChargeElaspedTime;

	// Called when the node enters the scene tree for the first time.
	public override void _Ready()
	{
		IsCharging = false;
		Momentum = 0.0f;
		ChargingNode = GetNode<Node2D>(ChargingNodePath);
		ChargeHitArea = GetNode<Area2D>(ChargeHitAreaPath);
	}

	public override void _PhysicsProcess(float delta)
	{
		base._PhysicsProcess(delta);

		if(IsCharging)
		{
			ChargeElaspedTime += delta;
			
			GD.Print(ChargingNode.Filename);


			KinematicBody2D ChargingBody = ChargingNode as KinematicBody2D;
			if(ChargingBody != null)
			{
				ChargingBody.MoveAndSlide(ChargeDirection * Momentum);
			}
			else
			{
				ChargingNode.GlobalPosition += ChargeDirection * Momentum * delta;
			}

			float CurrentDampening = MomentumDampeningCurve.Interpolate(Mathf.Min(ChargeElaspedTime, DampeningDuration) / DampeningDuration) * MaxDampening;
			Momentum -= CurrentDampening * delta;

			if(Momentum  <= 0.0f)
			{
				IsCharging = false;
				ChargeDirection = Vector2.Zero;

				CooldownAcc = CooldownDuration;
				IsCoolingDown = true;

				EmitSignal(nameof(charge_finished));
			}
		}
	}

	public override void _Process(float delta)
	{
		base._Process(delta);

		if(IsCoolingDown)
		{
			CooldownAcc -= delta;

			if(CooldownAcc <= 0.0f)
			{
				IsCoolingDown = false;

				if(CooldownDuration > 0.0f)
				{
					EmitSignal(nameof(cooldown_completed));
				}
			}
		}
	}

	public void Charge(Vector2 Direction)
	{
		if(!IsCharging && !IsCoolingDown)
		{
			IsCharging = true;
			Momentum = InitialMomentum;
			ChargeDirection = Direction;
			ChargeElaspedTime = 0.0f;
			CooldownAcc = 0.0f;

			EmitSignal(nameof(charge_started));
		}
	}

	public void Cancel()
	{
		if(IsCharging)
		{
			IsCharging = false;
			ChargeDirection = Vector2.Zero;

			// Starts cooldown
			CooldownAcc = CooldownDuration;
			IsCoolingDown = true;
			
			EmitSignal(nameof(charge_cancelled));
		}
	}
}
