using Godot;
using System;

public class ShootStaticTargetBrain : Brain
{
	[Export]
	public float ShootingRange = 300.0f;

    [Export]
    public Node2D Target;

	private enum States
	{
		None,
		Idle,
		Shooting,
	}

	private States CurrentState;
	private States PreviousState;

	// Called when the node enters the scene tree for the first time.
	public override void _Ready()
	{
		base._Ready();

		CurrentState = States.Idle;
		PreviousState = States.None;
	}

	protected override void OnMobChanged(Mob OldMob, Mob NewMob)
	{
		base.OnMobChanged(OldMob, NewMob);

		if(OldMob != null)
		{
			OldMob.Disconnect("shoot_finished", this, nameof(OnShootingFinished));
		}

		NewMob.Connect("shoot_finished", this, nameof(OnShootingFinished));
	}

	// Called every frame. 'delta' is the elapsed time since the previous frame.
	public override void _Process(float delta)
	{
		States OldState = CurrentState;

		switch(CurrentState)
		{
			case States.Idle:
			ProcessIdle(delta);
			break;
			case States.Shooting:
			ProcessShooting(delta);
			break;
		}

		PreviousState = OldState;
	}

	private void ProcessIdle(float delta)
	{
		if(IsSensingPlayer)
		{
			float DistanceToPlayer = (CurrentMob.GlobalPosition - SensedPlayer.GlobalPosition).Length();
            CurrentState = States.Shooting;
		}
	}

	private void ProcessShooting(float delta)
	{
		if(!IsSensingPlayer)
		{
			CurrentState = States.Idle;
			return;
		}

		if(PreviousState != States.Shooting)
		{
			if(!CurrentMob.TryShoot(Target.GlobalPosition))
			{
				CurrentState = States.Idle;
			}
		}
	}

	private void OnShootingFinished()
	{
		if(CurrentState == States.Shooting)
		{
			CurrentState = States.Idle;
		}
	}

	protected override void OnStartSensingPlayer(Player FoundPlayer)
	{
		base.OnStartSensingPlayer(FoundPlayer);
	}

	protected override void OnEndSensingPlayer(Player LostPlayer)
	{
		base.OnEndSensingPlayer(LostPlayer);
	}
}
