using Godot;
using System;

public class Brain : Node
{
    [Signal]
    public delegate void sensing_player_started();

    [Signal]
    public delegate void sensing_player_ended();

    private Mob ControlledMob;

    public Mob CurrentMob { get { return ControlledMob; }}

    private Player SensedPlayer_;

    public Player SensedPlayer { get { return SensedPlayer_; }}
    public bool IsSensingPlayer { get { return SensedPlayer_ != null; }}

    // Called when the node enters the scene tree for the first time.
    public override void _Ready()
    {
        base._Ready();
        
        SensedPlayer_ = null;
    }

    public void SetMob(Mob Mob)
    {
        Mob PreviousMob = ControlledMob;

        ControlledMob = Mob;

        if(ControlledMob != PreviousMob)
        {
            OnMobChanged(PreviousMob, ControlledMob);
        }
    }

    protected virtual void OnMobChanged(Mob OldMob, Mob NewMob)
    {
        if(OldMob != null)
        {
            OldMob.SensingArea.Disconnect("body_entered", this, nameof(OnSensingPlayerStarted));
            OldMob.SensingArea.Disconnect("body_exited", this, nameof(OnSensingPlayerEnded)); 
        }

        NewMob.SensingArea.Connect("body_entered", this, nameof(OnSensingPlayerStarted));
        NewMob.SensingArea.Connect("body_exited", this, nameof(OnSensingPlayerEnded));
    }

    private void OnSensingPlayerStarted(Node body)
    {
        if (body is Player player && SensedPlayer_ == null)
        {
            SensedPlayer_ = player;
            OnStartSensingPlayer(SensedPlayer_);
            EmitSignal(nameof(sensing_player_started));
        }
    }

    private void OnSensingPlayerEnded(Node body)
    {
        if (body is Player player)
        {
            OnEndSensingPlayer(SensedPlayer_);
            SensedPlayer_ = null;
            EmitSignal(nameof(sensing_player_ended));
        }
    }

    protected virtual void OnStartSensingPlayer(Player P)
    {

    }

    protected virtual void OnEndSensingPlayer(Player LostPlayer)
    {

    }
}
