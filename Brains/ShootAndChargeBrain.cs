using Godot;
using System;

public class ShootAndChargeBrain : Brain
{
	[Export]
	public float ShootingRange = 300.0f;

	// Curve to change charge weight base on distance to the player
	[Export]
	public Curve ChargingWeightCurve;

	private enum States
	{
		None,
		Idle,
		Shooting,
		Charging,
		Dying
	}

	private States CurrentState;
	private States PreviousState;

	// Called when the node enters the scene tree for the first time.
	public override void _Ready()
	{
		base._Ready();

		CurrentState = States.Idle;
		PreviousState = States.None;
	}

	protected override void OnMobChanged(Mob OldMob, Mob NewMob)
	{
		base.OnMobChanged(OldMob, NewMob);

		if(OldMob != null)
		{
			OldMob.Disconnect("charge_finished", this, nameof(OnChargeFinished));
			OldMob.Disconnect("shoot_finished", this, nameof(OnShootingFinished));
			OldMob.Disconnect("death_finished", this, nameof(OnDeathFinished));
		}

		NewMob.Connect("charge_finished", this, nameof(OnChargeFinished));
		NewMob.Connect("shoot_finished", this, nameof(OnShootingFinished));
		NewMob.Connect("death_finished", this, nameof(OnDeathFinished));
	}

	// Called every frame. 'delta' is the elapsed time since the previous frame.
	public override void _Process(float delta)
	{
		States OldState = CurrentState;

		switch(CurrentState)
		{
			case States.Idle:
			ProcessIdle(delta);
			break;
			case States.Shooting:
			ProcessShooting(delta);
			break;
			case States.Charging:
			ProcessCharging(delta);
			break;
			case States.Dying:
			ProcessDying(delta);
			break;
		}

		PreviousState = OldState;
	}

	private void ProcessIdle(float delta)
	{
		if(IsSensingPlayer)
		{
			float DistanceToPlayer = (CurrentMob.GlobalPosition - SensedPlayer.GlobalPosition).Length();
	
			if(DistanceToPlayer < ShootingRange)
			{
				float ChargingWeight = 1.0f;

				if(ChargingWeightCurve != null)
				{
					ChargingWeight = ChargingWeightCurve.Interpolate(DistanceToPlayer / ShootingRange);
				}

				if(ChargingWeight >= 1.0f || GD.Randf() > ChargingWeight)
				{
					CurrentState = States.Charging;
				}
				else
				{
					CurrentState = States.Shooting;
				}
			}
			else
			{
				CurrentState = States.Shooting;
			}
		}
	}

	private void ProcessShooting(float delta)
	{
		if(!IsSensingPlayer)
		{
			CurrentState = States.Idle;
			return;
		}

		if(PreviousState != States.Shooting)
		{
			if(!CurrentMob.TryShoot(SensedPlayer.GlobalPosition))
			{
				CurrentState = States.Idle;
			}
		}
	}

	private void OnShootingFinished()
	{
		if(CurrentState == States.Shooting)
		{
			CurrentState = States.Idle;
		}
	}

	private void ProcessCharging(float delta)
	{
		if(!IsSensingPlayer)
		{
			CurrentState = States.Idle;
			return;
		}

		if(PreviousState != States.Charging)
		{
			if(!CurrentMob.TryCharge(SensedPlayer.GlobalPosition))
			{
				CurrentState = States.Idle;
			}
		}
	}

	private void OnChargeFinished()
	{
		if(CurrentState == States.Charging)
		{
			CurrentState = States.Idle;
		}
	}

	private void ProcessDying(float delta)
	{
		if(PreviousState != States.Dying)
		{
			CurrentMob.Die();
		}
	}

	private void OnDeathFinished()
	{
		if(CurrentState == States.Dying)
		{
			this.QueueFree();
			CurrentMob.QueueFree();
		}
	}

	protected override void OnStartSensingPlayer(Player FoundPlayer)
	{
		base.OnStartSensingPlayer(FoundPlayer);
	}

	protected override void OnEndSensingPlayer(Player LostPlayer)
	{
		base.OnEndSensingPlayer(LostPlayer);
	}
}
