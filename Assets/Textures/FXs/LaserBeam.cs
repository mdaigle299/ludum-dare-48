using Godot;
using System;

public class LaserBeam : Node2D
{
    private AudioStreamPlayer2D ChargeSound;
    private AudioStreamPlayer2D FireingSound;
    private AudioStreamPlayer2D ShutdownSound;
    private Sprite Sprite;
    private Particles2D Particles2D;
    [Export] public float BeamLength = 1.0f;
    private float LastBeamLength;

    public override void _Ready()
    {
        ChargeSound = GetNode<AudioStreamPlayer2D>("ChargingSound");
        FireingSound = GetNode<AudioStreamPlayer2D>("FireingSound");
        ShutdownSound = GetNode<AudioStreamPlayer2D>("ShutdownSound");

        Sprite = GetNode<Sprite>("Sprite");
        Particles2D = GetNode<Particles2D>("Particles2D");

        ChargeSound.Connect("finished", this, nameof(OnChargingDone));
        ChargeSound.Play();

        SetBeamLength();
    }

    private void OnChargingDone()
    {
        FireingSound.Play();
    }

    public void Shutdown()
    {
        FireingSound.Stop();
        ShutdownSound.Play();
    }

    public override void _Process(float delta)
    {
        if (BeamLength != LastBeamLength)
        {
            SetBeamLength();
        }
    }

    private void SetBeamLength()
    {
        Particles2D.Position = Particles2D.Position * BeamLength;
        Sprite.RegionRect = new Rect2(Sprite.RegionRect.Position, new Vector2(Sprite.RegionRect.Size.x * BeamLength, Sprite.RegionRect.Size.y));
        LastBeamLength = BeamLength;
    }
}
