using Godot;
using System;
using System.Collections.Generic;

public class GameUI : Control
{
    private Label ObjectiveText;
    private Label KillCountText;
    private Label KillScoreText;
    private Tween KillScoreTextTween;
    private AudioStreamPlayer KillScoreTextFillUpSound;
    private AnimationPlayer AnimationPlayer;
    private HBoxContainer LifeContainer;
    [Export] public PackedScene HeartScene;
    [Export] public PackedScene HeartGoneScene;
    private int CurrentScore = 0;
    private int LastScore;
    private string LastObjectiveText;
    private List<Panel> CurrentHearts;

    public override void _Ready()
    {
        ObjectiveText = GetNode<Label>("ObjectiveText");
        KillCountText = GetNode<Label>("KillCountText");
        KillScoreText = GetNode<Label>("KillScoreText");
        KillScoreTextTween = GetNode<Tween>("KillScoreText/Tween");
        KillScoreTextFillUpSound = GetNode<AudioStreamPlayer>("KillScoreText/FillUpSound");
        AnimationPlayer = GetNode<AnimationPlayer>("AnimationPlayer");

        LifeContainer = GetNode<HBoxContainer>("LifeContainer");
    }

    public void SetObjectiveText(string text)
    {
        if (text != LastObjectiveText)
        {
            ObjectiveText.Text = text;
            LastObjectiveText = text;
        }
    }

    public void SetKillCountText(string text)
    {
        KillCountText.Text = text;
        AnimationPlayer.Stop();
        AnimationPlayer.Play("ComboUp");
    }

    public void UpdateKillScore(int score)
    {
        KillScoreTextTween.InterpolateProperty(this, "CurrentScore", null, score, 1f, Tween.TransitionType.Expo);
        KillScoreTextTween.Start();
        KillScoreTextFillUpSound.Play();
    }

    public void UpdateHearts(int count, int max)
    {
        var hearts = LifeContainer.GetChildren();
        foreach(var heart in hearts)
        {
            if (heart is Panel panel)
            {
                LifeContainer.RemoveChild(panel);
                panel.QueueFree();
            }
        }

        for (int i = 0; i < max; i++)
        {
            Panel heart = null;
            if (i < count)
            {
                heart = (Panel)HeartScene.Instance();
            }
            else
            {
                heart = (Panel)HeartGoneScene.Instance();
            }

            heart.Visible = true;
            LifeContainer.AddChild(heart);
        }
    }

    public override void _Process(float delta)
    {
        if (CurrentScore != LastScore)
        {
            KillScoreText.Text = $"Score: {CurrentScore}";
            LastScore = CurrentScore;
        }
    }

}
